package com.softserve.af.exceptions;

public class AutomationException extends RuntimeException{
    public AutomationException(String message) {
        super(message);
    }
}
