package com.softserve.af.webelement;

import com.softserve.af.actions.MouseAction;
import com.softserve.af.search.BaseSearch;
import com.softserve.af.search.NotSeleniumSearch;
import com.softserve.af.search.SearchType;
import com.softserve.af.search.SeleniumSearch;
import com.softserve.af.webdriver.AppBrowserDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Getter;
import org.openqa.selenium.WebDriver;

import java.util.Map;

@Getter
public class BaseElement {
    private String xpath;
    private ElementType elementType;
    private SearchType searchType;
    private boolean visible;
    private long wait;
    private BaseSearch elementSearch;
    private WebDriver webDriver;
    //TODO general driver type required

    public BaseElement(Map searchMap){
        this.xpath=(String) searchMap.get("xpath");
        if(searchMap.get("elementType")==null){
            this.elementType=ElementType.GENERIC;
        }else {
            this.elementType=(ElementType) searchMap.get("elementType");
        }
        if(searchMap.get("searchType")==null){
            this.searchType=SearchType.SELENIUM;
        }else {
            this.searchType=(SearchType) searchMap.get("searchType");
        }
        this.visible=(boolean) searchMap.get("visible");
        this.wait=(Long) searchMap.get("waitMilis");
        this.webDriver=((AppBrowserDriverManager) searchMap.get("driver")).getDriver();
    }

    BaseSearch createElementSearch(){//TODO think of abstract factory
        switch (searchType){
            case SELENIUM: elementSearch = new SeleniumSearch(webDriver);break;
            case NOT_SELENIUM: elementSearch = new NotSeleniumSearch();break;
        }
        return elementSearch;
    }

    public void mouseAction(MouseAction action){
        this.createElementSearch().findWebElement(this).mouseAction(action);
    }


    public boolean exists(){
        return this.createElementSearch().findWebElement(this).exists();
    }
}
