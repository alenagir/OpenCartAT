package com.softserve.af.webelement;

import com.softserve.af.actions.MouseAction;
import com.softserve.af.constants.WaitForEvent;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class SeleniumElement implements AutomationElement {
    @Getter
    @Setter
    private WebElement foundElement;
    @Getter
    @Setter
    private List<WebElement> foundElements;
    @NonNull
    private WebDriver driver;
    private Wait wait;
    Function<WebDriver,Boolean> pageLoad = (driver -> {
        String active = ((JavascriptExecutor)driver).executeScript("return document.readyState").toString();
        return active.equals("complete");
    });

    public void mouseAction(MouseAction actionType){
        Actions actions = new Actions(driver);
        switch (actionType){
            case CLICK:actions.click(this.foundElement);break;
            case DOUBLE_CLICK:actions.doubleClick(this.foundElement);break;
        }
        wait = new WebDriverWait(driver, WaitForEvent.PAGE_LOAD);//TODO think of correct place for this wait
        wait.until(pageLoad);
            actions.perform();

    }

    public boolean exists(){
        return this.foundElement.isDisplayed();
    }

}
