package com.softserve.af.webelement;

import com.softserve.af.actions.MouseAction;

public interface AutomationElement {
void mouseAction(MouseAction action);
boolean exists();

}
