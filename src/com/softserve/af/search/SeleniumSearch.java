package com.softserve.af.search;

import com.softserve.af.exceptions.AutomationException;
import com.softserve.af.webelement.AutomationElement;
import com.softserve.af.webelement.BaseElement;
import com.softserve.af.webelement.SeleniumElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SeleniumSearch extends BaseSearch{
    private WebDriver driver;
    private SeleniumElement seleniumElement;
    private List<WebElement> nativeElements;
    public SeleniumSearch(WebDriver driver){
        this.driver = driver;
    }



    public AutomationElement findWebElement(BaseElement element){
        seleniumElement = new SeleniumElement(driver);
        nativeElements = findSeleniumElements(element);
        if(nativeElements.isEmpty()){
            throw new AutomationException(String.format("Element %s not found", element.getXpath()));
        }
        if(nativeElements.size()==1){
            seleniumElement.setFoundElement(nativeElements.get(0));
        }else{
            seleniumElement.setFoundElements(nativeElements);
        }
        return seleniumElement;
    };

    private List<WebElement> findSeleniumElements(BaseElement element){
        return  driver.findElements(By.xpath(element.getXpath()));
    }
}
