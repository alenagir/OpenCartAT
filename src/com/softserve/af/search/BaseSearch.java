package com.softserve.af.search;

import com.softserve.af.webelement.AutomationElement;
import com.softserve.af.webelement.BaseElement;

public abstract class BaseSearch {
    public abstract AutomationElement findWebElement(BaseElement element);
}
