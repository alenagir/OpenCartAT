package com.softserve.af.search;

import com.softserve.af.webelement.AutomationElement;
import com.softserve.af.webelement.BaseElement;
import com.softserve.af.webelement.NotSeleniumElement;

public class NotSeleniumSearch extends BaseSearch{
    NotSeleniumElement notSeleniumElement;

    public NotSeleniumSearch(){
    }

    public void setNotSeleniumElement(NotSeleniumElement notSeleniumElement) {
        this.notSeleniumElement = notSeleniumElement;
    }

    public NotSeleniumElement getNotSeleniumElement() {
        return notSeleniumElement;
    }

    public AutomationElement findWebElement(BaseElement element){
        return notSeleniumElement;
    };

}
