package com.softserve.af.webdriver;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.util.HashMap;

public class AppChromeOptions extends ChromeOptions implements AppDriverOptions{

    public AppChromeOptions(boolean incognito, boolean headless, String windowSize/*, String downloadFilepath*/){//TODO нарушение принципа открытости-закрытости - передавать объект с опциями
        if(windowSize!=null) {
            this.addArguments("window-size="+windowSize);
        }else {
            this.addArguments("start-maximized");
        }
        if(headless){
            this.addArguments("headless");
        }
        if(incognito){
            this.addArguments("incognito");
        }
            this.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
       /* if(downloadFilepath!=null){
            HashMap<String, Object> chromePrefs = new HashMap<>();
            chromePrefs.put("download.default_directory", downloadFilepath);
            this.setExperimentalOption("prefs", chromePrefs);
        }*/


    }
}
