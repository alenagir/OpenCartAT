package com.softserve.af.webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class AppBrowserDriverManager {
    WebDriver appDriver;

    public WebDriver getDriver(){
        return  appDriver;
    }
    public AppBrowserDriverManager(BrowserName name, AppDriverOptions options){ //TODO think about one place for all options
        switch (name) {
            case CHROME: WebDriverManager.chromedriver().setup();
                        appDriver=new ChromeDriver((ChromeOptions) options);
                        break;
            case FIREFOX: WebDriverManager.firefoxdriver().setup();
                        appDriver=new FirefoxDriver((FirefoxOptions) options);
                        break;
            case EDGE: WebDriverManager.edgedriver().setup();
                        appDriver=new EdgeDriver((EdgeOptions) options);
                        break;
        }
    }

    public void quitDriver(){//TODO remove to
        appDriver.quit();

    }

    public void getUrl(String url){
        appDriver.get(url);
    }

}
