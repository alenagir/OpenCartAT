package com.softserve.af.actions;

public enum MouseAction {
    CLICK, HOVER, DOUBLE_CLICK
}
