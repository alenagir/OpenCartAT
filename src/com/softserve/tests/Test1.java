package com.softserve.tests;

import com.softserve.af.webdriver.AppBrowserDriverManager;
import com.softserve.af.webdriver.AppChromeOptions;
import com.softserve.af.webdriver.BrowserName;
import com.softserve.pom.home.HomeAction;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Test1 extends BaseTest{

    @BeforeClass
    void before_Test1(){
        System.out.println("Hello test1!");
    }

    @Test
    void test1(){
    homeAction
            .clickHeader()
            .verifyHeaderExists(true);

    }
}
