package com.softserve.tests;

import com.softserve.af.webdriver.AppBrowserDriverManager;
import com.softserve.af.webdriver.AppChromeOptions;
import com.softserve.af.webdriver.BrowserName;
import com.softserve.pom.home.HomeAction;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class BaseTest {
    private AppBrowserDriverManager driverManager;
    HomeAction homeAction;

    @Parameters({"incognito", "headless", "windowSize", "url"})
    @BeforeClass
    protected void basePreconditions(String incognito, String headless, String windowSize, String url){
        AppChromeOptions chromeOptions = new AppChromeOptions(Boolean.parseBoolean(incognito), Boolean.parseBoolean(headless), windowSize);
        driverManager = new AppBrowserDriverManager(BrowserName.CHROME, chromeOptions);
        driverManager.getUrl(url);
        homeAction = new HomeAction(driverManager);
    }
}
