package com.softserve.pom.home;

import com.softserve.af.search.BasePage;
import com.softserve.af.search.SearchType;
import com.softserve.af.webdriver.AppBrowserDriverManager;
import com.softserve.af.webelement.BaseElement;
import com.softserve.af.webelement.ElementType;

import java.util.HashMap;
import java.util.Map;

public class HomePage extends BasePage {
    BaseElement header;
    AppBrowserDriverManager driverManager;

    public HomePage(AppBrowserDriverManager driverManager) {
        this.driverManager = driverManager;
    }

    public BaseElement getHeader(){
        Map searchMap = new HashMap();
        searchMap.put("xpath","//h1[.='Your Stor']");
        searchMap.put("visible",true);
        searchMap.put("waitMilis",500L);
        searchMap.put("elementType", ElementType.GENERIC);
        searchMap.put("driver", driverManager); //TODO add general type for different drivers
        searchMap.put("searchType", SearchType.SELENIUM); //TODO add general type for different drivers
      return getBaseElement(searchMap);
    };

}
