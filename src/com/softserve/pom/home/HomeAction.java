package com.softserve.pom.home;

import com.softserve.af.actions.MouseAction;
import com.softserve.af.webdriver.AppBrowserDriverManager;
import org.testng.Assert;

public class HomeAction {
    HomePage homePage;

    public HomeAction(AppBrowserDriverManager driverManager){
        homePage = new HomePage(driverManager);
    }

    public HomeAction clickHeader(){
            homePage.getHeader().mouseAction(MouseAction.CLICK);
        return this;
    }

    public HomeAction verifyHeaderExists(boolean expected){
        boolean actual = homePage.getHeader().exists();
        Assert.assertEquals(actual, expected);
        return this;
    }

}
